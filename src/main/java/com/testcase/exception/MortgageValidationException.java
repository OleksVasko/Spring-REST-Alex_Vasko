package com.testcase.exception;


public class MortgageValidationException extends RuntimeException{

    public MortgageValidationException(String message) {
        super(message);
    }

}
