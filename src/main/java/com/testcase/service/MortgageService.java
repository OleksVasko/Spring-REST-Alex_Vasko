package com.testcase.service;

import com.testcase.model.MortgageRate;

import java.util.List;

public interface MortgageService {

    long createMortgageEntry(MortgageRate entry);

    void updateMortgageEntry(MortgageRate entry);

    MortgageRate readMortgageEntry(long id);

    MortgageRate readMortgageEntryByMaturity(int maturityYears);

    void deleteMortgageEntry(long id);

    List<MortgageRate> readAllMortgageEntries();
}
