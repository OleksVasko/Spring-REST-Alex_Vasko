package com.testcase.service;

import com.testcase.model.MortgageDetails;

public interface CalculationService  {

    MortgageDetails calcMortgage(MortgageDetails mortgage);
}
