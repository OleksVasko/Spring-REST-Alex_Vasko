package com.testcase.service;

import com.testcase.exception.DataValidationException;
import com.testcase.exception.MortgageValidationException;
import com.testcase.model.MortgageDetails;
import com.testcase.model.MortgageRate;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class ValidationService {


    private final static Logger LOG = Logger.getLogger(ValidationService.class);

    protected void validateMortgageEntry(MortgageRate rate) throws IllegalArgumentException {
        if (rate.getMaturityPeriod() <= 0) {
            LOG.error("Maturity cannot be negative or 0");
            throw new DataValidationException("Current maturity is set to " + rate.getMaturityPeriod());
        } else if (rate.getInterestRate() <= 0f || rate.getInterestRate() > 99.9f) {
            LOG.error("Interest rate should be within 0 and 99.9 boundaries");
            throw new DataValidationException("Current interest rate is set to " + rate.getInterestRate());
        } else if (rate.getLastModified().isAfter(LocalDate.now())) {
            LOG.error("Wrong time stamp for \"LastModified\"  field");
        }
    }

    protected boolean isMortgageAvailable(MortgageDetails mortgage) {

        if (mortgage.getLoanAmount().compareTo(mortgage.getTotalHousePrice()) > 0) {
            throw new MortgageValidationException("Loan amount cannot be bigger than total house price. Loan amount is "
                    + mortgage.getLoanAmount() + ", total house price is " + mortgage.getTotalHousePrice());
        }

        if (mortgage.getAnnualIncome().multiply(new BigDecimal(4)).compareTo(mortgage.getLoanAmount()) < 0) {
            throw new MortgageValidationException("Loan amount cannot be bigger then 4 annual incomes. Loan amount is "
                    + mortgage.getLoanAmount() + ", total value of 4 annual incomes is "
                    + mortgage.getAnnualIncome().multiply(new BigDecimal(4)));
        }
        return true;
    }

    public void validateMortgageDetails(MortgageDetails mortgage) {
        BigDecimal zeroValue = new BigDecimal(0);
        if (mortgage.getLoanAmount().compareTo(zeroValue) == 0 ||
                mortgage.getAnnualIncome().compareTo(zeroValue) == 0 ||
                mortgage.getTotalHousePrice().compareTo(zeroValue) == 0 ||
                mortgage.getMaturity() == 0) {

            throw new DataValidationException(
                    "Non of the values should be 0. Loan amount is " + mortgage.getLoanAmount().setScale(0, RoundingMode.UP) + ". Maturity in years is  "
                            + mortgage.getMaturity() + ".  Annual income is " + mortgage.getAnnualIncome().setScale(0, RoundingMode.UP)
                            + ".  Total house price is " + mortgage.getTotalHousePrice().setScale(0, RoundingMode.UP));
        }
    }
}
