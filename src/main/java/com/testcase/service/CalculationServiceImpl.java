package com.testcase.service;

import com.testcase.model.MortgageDetails;
import com.testcase.model.MortgageRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CalculationServiceImpl implements CalculationService {


    private ValidationService validationService;
    private MortgageService mortgageService;

    @Autowired
    public CalculationServiceImpl(ValidationService validationService, MortgageService mortgageService) {
        this.validationService = validationService;
        this.mortgageService = mortgageService;
    }

    @Override
    public MortgageDetails calcMortgage(MortgageDetails mortgage) {
        validationService.validateMortgageDetails(mortgage);
        MortgageDetails details = new MortgageDetails();
        details.setMaturity(mortgage.getMaturity());
        details.setTotalHousePrice(mortgage.getTotalHousePrice());
        details.setLoanAmount(mortgage.getLoanAmount());
        details.setAnnualIncome(mortgage.getAnnualIncome());

        details.setAvailable(validationService.isMortgageAvailable(mortgage));
        BigDecimal monthlyPayment = calculateMonthlyPayment(mortgage.getLoanAmount(), mortgage.getMaturity());
        details.setMonthlyPayment(monthlyPayment);
        details.setTotalAmountToPay(monthlyPayment.multiply(new BigDecimal(mortgage.getMaturity()).multiply(new BigDecimal(12))).setScale(2, RoundingMode.UP));
        return details;
    }

    public BigDecimal calculateMonthlyPayment(BigDecimal loanAmount, int maturity) {
        MortgageRate mortgageRate = mortgageService.readMortgageEntryByMaturity(maturity);
        float annualInterestRate = mortgageRate.getInterestRate();
        float monthlyInterestRate = annualInterestRate / 12;
        BigDecimal bdMonthlyInterestRate = new BigDecimal(monthlyInterestRate);
        return bdMonthlyInterestRate.multiply(new BigDecimal(1).add(bdMonthlyInterestRate).pow(maturity * 12))
                .divide(new BigDecimal(1).add(bdMonthlyInterestRate).pow(maturity * 12).subtract(new BigDecimal(1)), RoundingMode.UP)
                .multiply(loanAmount).setScale(2, RoundingMode.UP);
    }
}
