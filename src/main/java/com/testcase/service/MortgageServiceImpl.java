package com.testcase.service;

import com.testcase.dao.MortgageDao;
import com.testcase.exception.DataValidationException;
import com.testcase.model.MortgageRate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MortgageServiceImpl implements MortgageService {

    private final static Logger LOG = Logger.getLogger(ValidationService.class);

    private MortgageDao mortgageDao;
    private ValidationService validationService;

    public MortgageServiceImpl(){}

    @Autowired
    public MortgageServiceImpl(MortgageDao mortgageDao, ValidationService validationService) {
        this.mortgageDao = mortgageDao;
        this.validationService = validationService;
    }

    public long createMortgageEntry(MortgageRate entry) {
        try {
            validationService.validateMortgageEntry(entry);
            return mortgageDao.createMortgageEntry(entry);
        }catch (IllegalArgumentException ex){
            LOG.error(new DataValidationException("Invalid data provided", ex));
        }
        return -1;
    }

    public void updateMortgageEntry(MortgageRate entry) {
        mortgageDao.updateMortgageEntry(entry);
    }

    public MortgageRate readMortgageEntry(long id) {
        return mortgageDao.readMortgageEntryById(id);
    }

    @Override
    public MortgageRate readMortgageEntryByMaturity(int maturityYears) {
        return mortgageDao.readMortgageEntryByMaturity(maturityYears);
    }

    public void deleteMortgageEntry(long id) {
        mortgageDao.deleteMortgageEntry(id);
    }

    public List<MortgageRate> readAllMortgageEntries() {
        return mortgageDao.readAllMortgageEntries();
    }
}
