package com.testcase.controller;


import com.testcase.model.MortgageDetails;
import com.testcase.model.MortgageRate;
import com.testcase.service.CalculationService;
import com.testcase.service.MortgageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class BaseController {

    private MortgageService mortgageService;
    private CalculationService calculationService;

    @Autowired
    public BaseController(MortgageService service, CalculationService calculationService) {
        this.mortgageService = service;
        this.calculationService = calculationService;
    }


    @RequestMapping(value = "/interest-rates", method = {RequestMethod.GET})
    public @ResponseBody
    List<MortgageRate> getAllRates() {
        return mortgageService.readAllMortgageEntries();
    }

    @RequestMapping(value = "/mortgage-check/{income}/{maturity}/{loanValue}/{homeValue}", method = {RequestMethod.POST})
    public @ResponseBody
    MortgageDetails checkMortGageAvailability(@PathVariable Integer income, @PathVariable Integer maturity,
                                              @PathVariable Integer loanValue, @PathVariable Integer homeValue) {
        MortgageDetails mortgageDetails = new MortgageDetails(new BigDecimal(income), new BigDecimal(homeValue), new BigDecimal(loanValue), maturity);
        return calculationService.calcMortgage(mortgageDetails);
    }


    @RequestMapping("/")
    public String goHome() {
        System.out.println("========GoHome============");
        return "index";
    }
}
