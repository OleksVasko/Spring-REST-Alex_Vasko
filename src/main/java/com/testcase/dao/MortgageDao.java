package com.testcase.dao;

import com.testcase.model.MortgageRate;

import java.util.List;

public interface MortgageDao {

    long createMortgageEntry(MortgageRate entry);

    void updateMortgageEntry(MortgageRate entry);

    MortgageRate readMortgageEntryById(long id);

    MortgageRate readMortgageEntryByMaturity(int maturityYears);

    void deleteMortgageEntry(long id);

    List<MortgageRate> readAllMortgageEntries();
}
