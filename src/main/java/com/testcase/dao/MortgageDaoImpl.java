package com.testcase.dao;

import com.testcase.model.MortgageRate;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository(value = "MortgageDao")
@Transactional
public class MortgageDaoImpl extends AbstractDao implements MortgageDao {

    private final static Logger LOG = Logger.getLogger(MortgageDaoImpl.class);

    public long createMortgageEntry(MortgageRate mortgage) {
        LOG.info("Creating mortgage entry");
        return (Integer) getCurrentSession().save(mortgage);

    }

    public void updateMortgageEntry(MortgageRate entry) {
        getCurrentSession().merge(entry);
    }

    public MortgageRate readMortgageEntryById(long id) {
        return ((MortgageRate) createBlankCriteria(MortgageRate.class).add(Restrictions.eq("id", id)).uniqueResult());
    }

    public MortgageRate readMortgageEntryByMaturity(int maturityYears) {
        return ((MortgageRate) createBlankCriteria(MortgageRate.class).add(Restrictions.eq("maturityPeriod", maturityYears)).uniqueResult());
    }

    public void deleteMortgageEntry(long id) {
        MortgageRate mortgageRate = readMortgageEntryById(id);
        if(mortgageRate != null){
            getCurrentSession().delete(mortgageRate);
        }
    }

    public List<MortgageRate> readAllMortgageEntries() {
        return (List<MortgageRate>) createBlankCriteria(MortgageRate.class).list();
    }
}
