package com.testcase;

import com.testcase.model.MortgageRate;
import com.testcase.service.MortgageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("springAppContext.xml");
        MortgageService mortgageService = (MortgageService) ctx.getBean("mortgageServiceImpl");

        MortgageRate mortgageRate = new MortgageRate(1, 0.035f);
        mortgageService.createMortgageEntry(mortgageRate);

        List<MortgageRate> mortgageRates = mortgageService.readAllMortgageEntries();

        for (MortgageRate rate : mortgageRates) {
            System.out.println(rate);
        }
    }
}
