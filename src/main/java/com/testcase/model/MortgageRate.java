package com.testcase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "MORTGAGE")
public class MortgageRate {

    public MortgageRate() {
    }

    @Id
    private int id;

    @Column(name = "MATURITY")
    private  int maturityPeriod;

    @Column(name = "INTEREST")
    private  float interestRate;

    @Column(name = "DATE_MODIFIED")
    private LocalDate lastModified;

    public MortgageRate(int maturityPeriod, float interestRate) {
        this.maturityPeriod = maturityPeriod;
        this.interestRate = interestRate;
        this.lastModified = LocalDate.now();
    }

    public int getId() {
        return id;
    }

    public int getMaturityPeriod() {
        return maturityPeriod;
    }

    public float getInterestRate() {
        return interestRate;
    }

    public LocalDate getLastModified() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        lastModified.format(formatter);
        return lastModified;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMaturityPeriod(int maturityPeriod) {
        this.maturityPeriod = maturityPeriod;
    }

    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }

    public void setLastModified(LocalDate lastModified) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        lastModified.format(formatter);
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "MortgageRate{" +
                "id=" + id +
                ", maturityPeriod=" + maturityPeriod +
                ", interestRate=" + interestRate +
                ", lastModified=" + lastModified +
                '}';
    }
}
