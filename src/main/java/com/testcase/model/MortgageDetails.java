package com.testcase.model;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Entity
public class MortgageDetails {

    private BigDecimal totalHousePrice;
    private BigDecimal loanAmount;
    private int maturity;
    private boolean isAvailable;
    private BigDecimal monthlyPayment;
    private BigDecimal totalAmountToPay;
    private BigDecimal annualIncome;

    public MortgageDetails(){}

    public MortgageDetails(BigDecimal annualIncome, BigDecimal totalHousePrice, BigDecimal loanAmount, int maturity){
        this.annualIncome = annualIncome;
        this.totalHousePrice = totalHousePrice;
        this.loanAmount = loanAmount;
        this.maturity = maturity;
    }



    public BigDecimal getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(BigDecimal annualIncome) {
        this.annualIncome = annualIncome;
    }

    public BigDecimal getTotalHousePrice() {
        return totalHousePrice;
    }

    public void setTotalHousePrice(BigDecimal totalHousePrice) {
        this.totalHousePrice = totalHousePrice;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public int getMaturity() {
        return maturity;
    }

    public void setMaturity(int maturity) {
        this.maturity = maturity;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public BigDecimal getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(BigDecimal monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public BigDecimal getTotalAmountToPay() {
        return totalAmountToPay;
    }

    public void setTotalAmountToPay(BigDecimal totalAmountToPay) {
        this.totalAmountToPay = totalAmountToPay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MortgageDetails)) return false;
        MortgageDetails that = (MortgageDetails) o;
        return maturity == that.maturity &&
                isAvailable == that.isAvailable &&
                isEqualWithTolerance(totalHousePrice, that.totalHousePrice) &&
                isEqualWithTolerance(loanAmount, that.loanAmount) &&
                isEqualWithTolerance(monthlyPayment, that.monthlyPayment) &&
                isEqualWithTolerance(totalAmountToPay, that.totalAmountToPay) &&
                isEqualWithTolerance(annualIncome, that.annualIncome);
    }

    @Override
    public int hashCode() {

        return Objects.hash(totalHousePrice, loanAmount, maturity, isAvailable, monthlyPayment, totalAmountToPay, annualIncome);
    }

    @Override
    public String toString() {
        return "MortgageDetails{" +
                "totalHousePrice=" + totalHousePrice +
                ", loanAmount=" + loanAmount +
                ", maturity=" + maturity +
                ", isAvailable=" + isAvailable +
                ", monthlyPayment=" + monthlyPayment +
                ", totalAmountToPay=" + totalAmountToPay +
                ", annualIncome=" + annualIncome +
                '}';
    }

    private boolean isEqualWithTolerance (BigDecimal bd1, BigDecimal bd2) {
        return bd1.compareTo(bd2) == 0 || bd1.subtract(bd2).abs().compareTo(new BigDecimal(0.1).setScale(1, RoundingMode.UP)) <= 0;
    }
}
