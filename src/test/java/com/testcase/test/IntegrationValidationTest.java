package com.testcase.test;


import com.testcase.exception.DataValidationException;
import com.testcase.exception.MortgageValidationException;
import com.testcase.model.MortgageDetails;
import com.testcase.model.MortgageRate;
import com.testcase.service.CalculationService;
import com.testcase.service.MortgageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Arrays;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"/WEB-INF/applicationContext.xml"})
public class IntegrationValidationTest {


    @Autowired
    private CalculationService calculationService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void getAllRatesTest() throws Exception {

        mockMvc.perform(get("/api/interest-rates"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(20)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].maturityPeriod", is(1)))
                .andExpect(jsonPath("$[0].interestRate", is(0.02)))
                .andExpect(jsonPath("$[5].id", is(6)))
                .andExpect(jsonPath("$[5].maturityPeriod", is(6)))
                .andExpect(jsonPath("$[5].interestRate", is(0.025)))
                .andExpect(jsonPath("$[10].id", is(11)))
                .andExpect(jsonPath("$[10].maturityPeriod", is(11)))
                .andExpect(jsonPath("$[10].interestRate", is(0.035)))
                .andExpect(jsonPath("$[15].id", is(16)))
                .andExpect(jsonPath("$[15].maturityPeriod", is(16)))
                .andExpect(jsonPath("$[15].interestRate", is(0.045)));
    }

    @Test
    public void checkMortgageAvailableTest() throws Exception {

        mockMvc.perform(post("/api/mortgage-check/50000/15/200000/300000"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.totalHousePrice", is(300000)))
                .andExpect(jsonPath("$.loanAmount", is(200000)))
                .andExpect(jsonPath("$.maturity", is(15)))
                .andExpect(jsonPath("$.monthlyPayment", is(1429.77)))
                .andExpect(jsonPath("$.totalAmountToPay", is(257358.6)))
                .andExpect(jsonPath("$.annualIncome", is(50000)))
                .andExpect(jsonPath("$.available", is(true)));

    }


    @Test(expected = MortgageValidationException.class)
    public void checkIncomeToSmallTest() throws Throwable {
        try {
            mockMvc.perform(post("/api/mortgage-check/10000/15/200000/300000"));
        } catch (Exception e){
            System.out.println(e.getCause().getMessage());
            throw e.getCause();
        }
    }


    @Test(expected = MortgageValidationException.class)
    public void checkLoanTooBigTest() throws Throwable {
        try {
            mockMvc.perform(post("/api/mortgage-check/100000/15/500000/300000"));
        } catch (Exception e){
            System.out.println(e.getCause().getMessage());
            throw e.getCause();
        }
    }

    @Test
    public void calculateMortgageTest(){
        MortgageDetails details = new MortgageDetails();
        details.setMaturity(15);
        details.setTotalHousePrice(new BigDecimal(300000).setScale(2, RoundingMode.UP));
        details.setLoanAmount(new BigDecimal(200000).setScale(2, RoundingMode.UP));
        details.setAnnualIncome(new BigDecimal(50000).setScale(2, RoundingMode.UP));
        MortgageDetails mortgageDetails = calculationService.calcMortgage(details);
        details.setAvailable(true);
        details.setMonthlyPayment(new BigDecimal(1429.77).setScale(2, RoundingMode.UP));
        details.setTotalAmountToPay(new BigDecimal(257358.6).setScale(2,RoundingMode.UP));
        System.out.println(details);
        System.out.println(mortgageDetails);
        assertEquals(details, mortgageDetails);
    }

    @Test(expected = DataValidationException.class)
    public void zeroValuesTest() throws Throwable {
        try {
            mockMvc.perform(post("/api/mortgage-check/0/15/0/300000"));
        } catch (Exception e){
            System.out.println(e.getCause().getMessage());
            throw e.getCause();
        }
    }

}
